class Import < ApplicationRecord
  has_one_attached :file
  has_many :orders
end
