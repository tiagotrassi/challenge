require 'csv'

class ImportService

  def initialize(import_params)
    @import = Import.new(import_params)
  end

  def execute

    parse_file
    create_resources
  end

  private

  def parse_file
    file = @import.file.download
    @parsed_file = CSV.parse(file.force_encoding('UTF-8'), { encoding: 'UTF-8', col_sep: "\t" })
    @parsed_file.delete_at(0)
  end

  def create_resources
    total_value = 0
    @import.transaction do
      @import.save!
      @parsed_file.each do |line|
        buyer = create_buyer(line)
        provider = create_provider(line)
        product = create_product(line, provider)
        price = create_price(line, product)
        order = create_order(line, product, buyer, price, @import)
        total_value += order.total_price
      end
      @import.update_attributes!(total_value: total_value)
    end
    @import
  end

  def create_provider(line)
    Provider.create!(name: line[5])
  end

  def create_buyer(line)
    Buyer.create!(name: line[0], address: line[4])
  end

  def create_product(line, provider)
    Product.create!(description: line[1], provider_id: provider.id)
  end

  def create_price(line, product)
    value = (line[2].to_f * 100).to_i
    Price.create!(value: value, product_id: product.id)
  end

  def create_order(line, product, buyer, price, import)
    quantity = line[3].to_i
    total_price = quantity * price.value
    Order.create!(
      quantity: quantity,
      product_id: product.id,
      import_id: import.id,
      buyer_id: buyer.id,
      unit_price: price.value,
      total_price: total_price
    )
  end

end
