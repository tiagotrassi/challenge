module ImportsHelper
  def currency(value)
    number_to_currency(value.to_f / 100, precision: 2)
  end
end
