class ImportsController < ApplicationController
  before_action :set_import, only: [:show, :edit, :update, :destroy]

  def index
    @imports = Import.all
  end

  def show
  end

  def new
    @import = Import.new
  end

  def create
    import_service = ImportService.new(import_params)
    @import = import_service.execute

    respond_to do |format|
      format.html { redirect_to @import, notice: t('import_success') }
    end
  rescue
    redirect_to :root, notice: t('import_fail')
  end

  private

  def set_import
    @import = Import.eager_load(orders: [:buyer, product: [:prices, :provider]]).find(params[:id])
  end

  def import_params
    params.require(:import).permit(:file)
  end

end
