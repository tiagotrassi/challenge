Rails.application.routes.draw do
  resources :imports
  root to: 'imports#index'

end
