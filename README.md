Requisitos:

* Ruby 2.5.0

* Rails 5.2.3

* SQLite3

Ao efetuar checkout do projeto, executar os seguintes comandos na raiz do projeto:
```
$ bundle install
$ rake db:create db:migrate
```
Para executar os testes basta executar o seguinte comando:
```
$ rails test
```
Para iniciar a aplicacao, executar o comando:
```
$ rails server
```
Aplicacao estará disponivel em:
```
http://localhost:3000/
```
